package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testaSomaDeDoisNumerosInteiros(){
        int resulatdo = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resulatdo);
    }

    @Test
    public void testaSomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);

    }

    @Test
    public void testaMultiplicacaoDeDoisNumerosInteiros(){
        int resultado = calculadora.multiplicacao(4, 7);

        Assertions.assertEquals(28, resultado);
    }

    @Test
    public void testaMultiplicacaoDeDoisNumerosNegativos(){
        int resultado = calculadora.multiplicacao(-4, -7);

        Assertions.assertEquals(28, resultado);
    }

    @Test
    public void testaMultiplicacaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.multiplicacao(3.4, 3.9);

        Assertions.assertEquals(13.26, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosInteiros(){
        double resultado = calculadora.divisao(4, 8);

        Assertions.assertEquals(2, resultado);

    }

    @Test
    public void testaDivisaoDeDoisNumerosNegativos(){
        double resultado = calculadora.divisao(-4, -8);

        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.divisao(2.2, 10.4);

        Assertions.assertEquals(4.72, resultado);
    }
}
